Important Git Commands
git config --global:
	git config --global user.name 'name' : set global user.name value
	git config --global user.name : get global user.name
	git config --global user.email 'email' : set global user.email value
	git config --global user.email : get global user.email
	
git init :
	git init : initialize git repository in current directory
	git init dir : initialize git repository in dir

rm -rf .git :
	To remove git repository

git clone url foldername:
	To download files from url in foldername folder.

git status :
	To check the status of files untracked, unmodified/staged, modified

git add :
	git add * : adds to tracked or staged all the files including ignored
	git add . : adds to tracked alll the files except ignored
	git add filename : adds file to tracked

git restore :
	git restore --staged filename: to unstage the file

git diff :
	To display difference b/w modified and staged

git diff --cached/ --staged :
	To display diference b/w staged and previous commit

git log :
	git log : To display log of all commits
	git log -p : Display log of all commits with difference
	git log -p -n : display last n commits log with diff // n = 1,2,3....
	git log --stat : display commits with less details of difference 
	git log --pretty=oneline : shows all commits with each commit in one line
	git log --pretty=short : shows all commits in short (Author(name, Email), Comment)
	git log --pretty=full : shows all commits (Author(name, Email),commit(name, email), Comment)
	git log --since=2.days : shows all commits of last 2 days
	git log --since=2.weeks : shows all commits of last 2 weeks
	git log --since=2.years : shows all commits of last 2 years

git commit:
	git commit : to commit with vim editor
	git commit --amend : change the last commit with new one
	git commit -m 'comment': to commit without opening vim editor
	git commit -a -m 'comment': to commit without staged ( untracked are not committed )

git rm :
	git rm filename : deletes the file
	git rm --cached filename : untrack the file

git mv :
	git mv filename1 filename2 : rename filename1 to filename2

git checkout :
	git checkout -- filename : to get the previous committed version of the file
	git checkout -f : to get previous committed version of all the files 
	git checkout -b branchname : to create and switch to new branch branchname
	git checkout branchname : to switch to branchname

// to create shortcut of commands
git alias :
	git config --global alias.shortname 'subcommand1 subcommand2 '
	example : git config --global alias.st status => git status
	example : git config --global alias.unstage 'restore --staged --' => git unstage filename

git branch : to get all the branches 

git merge branchname : to merge branchname with current branch

git push -u origin master : to push into master branch

git remote -v

git remote

git remote rename orgin origin-old

git remote rename old-origin