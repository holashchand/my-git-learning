Important Git Commands
git config --global:
	git config --global user.name 'name' : set global user.name value
	git config --global user.name : get global user.name
	git config --global user.email 'email' : set global user.email value
	git config --global user.email : get global user.email
	
git init :
	git init : initialize git repository in current directory
	git init dir : initialize git repository in dir

git status :
	To check the status of files untracked, unmodified/staged, modified

git add :
	git add * : adds to tracked or staged all the files including ignored
	git add . : adds to tracked alll the files except ignored
	git add filename : adds file to tracked

git restore :
	git restore --staged filename: to unstage the file

git diff :
	To display difference b/w modified and staged

git diff --cached/ --staged :
	To display diference b/w staged and previous commit

git log :
	To display log of all commits

git commit:
	git commit : to commit with vim editor
	git commit -m 'comment': to commit without opening vim editor
	git commit -a -m 'comment': to commit without staged ( untracked are not committed )

git rm :
	git rm filename : deletes the file

git mv :
	git mv filename1 filename2 : rename filename1 to filename2